
# Microfrontends Arch

This is a live challenge to check your architectural analysis experience, please:

- Run the microfrontend app
- Check the docs for the JsonPlaceholder [REST API](https://jsonplaceholder.typicode.com/)  
- Add/Modify one of the microfrontends to list all the posts/comments from an user (search the user by username with a simple form). Implement the [BFF](https://blog.bitsrc.io/bff-pattern-backend-for-frontend-an-introduction-e4fa965128bf) pattern to compose the several API calls that you have to do. 
- Add some simple unit/integration tests to your solution
- Add a simple architecture diagram/explanation of your solution
- Make a pull request to this repo (branch develop) with your answers
- Please, don't use chatGPT...

[Original repo](https://github.com/santhoshvernekar/single-spa-microfrontend-angular-vue-react)

The repository demonstrate microfrontends coexisting within a single page using Angular, React and Vue sample apps. It uses [single-spa](https://single-spa.js.org) to pull this off.

# Build & run

Inside of each folder (*root-config, homepage, navbar, react, angular, vue*) install the dependencies, build and run. 

```
npm i
npm npm build
npm start
```

Open http://localhost:9000 in a browser.


